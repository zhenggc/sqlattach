object frmAttach: TfrmAttach
  Left = 0
  Top = 0
  Caption = 'Sql2008'#25968#25454#24211#25209#37327#38468#21152#24037#20855
  ClientHeight = 528
  ClientWidth = 706
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 384
    Top = 59
    Width = 80
    Height = 16
    Caption = #26381#21153#22120#21517#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 384
    Top = 99
    Width = 80
    Height = 16
    Caption = #29992' '#25143' '#21517#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 384
    Top = 139
    Width = 72
    Height = 16
    Caption = #23494'    '#30721':'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
  end
  object SBAttachDb: TSpeedButton
    Left = 608
    Top = 108
    Width = 90
    Height = 47
    Caption = #38468#21152#25968#25454#24211
    OnClick = SBAttachDbClick
  end
  object Label4: TLabel
    Left = 384
    Top = 187
    Width = 96
    Height = 16
    Caption = #25968#25454#24211#36335#24452#65306
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #23435#20307
    Font.Style = []
    ParentFont = False
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 286
    Width = 690
    Height = 234
    DataSource = DataSource1
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object DBNavigator1: TDBNavigator
    Left = 280
    Top = 255
    Width = 240
    Height = 25
    DataSource = DataSource1
    TabOrder = 1
  end
  object FileName: TFileListBox
    Left = 151
    Top = 129
    Width = 227
    Height = 120
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    ItemHeight = 13
    Mask = '*.mdf'
    MultiSelect = True
    TabOrder = 2
  end
  object Dir: TDirectoryListBox
    Left = 0
    Top = 33
    Width = 145
    Height = 216
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    TabOrder = 3
    OnChange = DirChange
  end
  object Driver: TDriveComboBox
    Left = 0
    Top = 8
    Width = 145
    Height = 19
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    TabOrder = 4
    OnChange = DriverChange
  end
  object EdtServer: TEdit
    Left = 472
    Top = 56
    Width = 121
    Height = 24
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #23435#20307
    Font.Style = []
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    ParentFont = False
    TabOrder = 5
    Text = '.'
  end
  object EdtUser: TEdit
    Left = 472
    Top = 96
    Width = 121
    Height = 24
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #23435#20307
    Font.Style = []
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    ParentFont = False
    TabOrder = 6
    Text = 'sa'
  end
  object EdtPass: TEdit
    Left = 472
    Top = 136
    Width = 121
    Height = 24
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #23435#20307
    Font.Style = []
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 7
  end
  object BtnConnect: TButton
    Left = 608
    Top = 53
    Width = 90
    Height = 44
    Caption = #36830#25509
    TabOrder = 8
    OnClick = BtnConnectClick
  end
  object Edtpath: TEdit
    Left = 472
    Top = 179
    Width = 226
    Height = 24
    Hint = #24403#25214#19981#21040#30446#24405#26102#21487#21452#20987#27492#22788#36873#20013#30446#24405#65281
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #23435#20307
    Font.Style = []
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
    OnChange = EdtpathChange
    OnDblClick = OnDblClick
  end
  object FileListBox1: TFileListBox
    Left = 151
    Top = 8
    Width = 227
    Height = 85
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    ItemHeight = 13
    Mask = '*.mdf'
    MultiSelect = True
    TabOrder = 10
  end
  object Button1: TButton
    Left = 176
    Top = 99
    Width = 73
    Height = 24
    Caption = #28155#21152
    TabOrder = 11
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 280
    Top = 99
    Width = 73
    Height = 24
    Caption = #28165#38500
    TabOrder = 12
    OnClick = Button2Click
  end
  object AdoCnn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;User ID=sa;Initial Catalog=master;Data Source=.;Use Pro' +
      'cedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workst' +
      'ation ID=ZHENGGC;Use Encryption for Data=False;Tag with column c' +
      'ollation when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 648
    Top = 16
  end
  object ADOCommand1: TADOCommand
    Parameters = <>
    Left = 520
    Top = 16
  end
  object ADOQAttach: TADOQuery
    Connection = AdoCnn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'DBCC checkprimaryfile(N'#39'E:\data\CHSS_BS_HisClinci_XJ.MDF'#39', 2)')
    Left = 456
    Top = 16
  end
  object DataSource1: TDataSource
    DataSet = ADOQAttach
    Left = 368
    Top = 16
  end
  object ADOQAdd: TADOQuery
    Connection = AdoCnn
    Parameters = <>
    Left = 408
    Top = 16
  end
  object OpDlg: TOpenDialog
    Left = 624
    Top = 232
  end
end

program SqlAttach;

uses
  Forms,
  Unit1 in 'Unit1.pas' {frmAttach},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Silver');
  Application.CreateForm(TfrmAttach, frmAttach);
  Application.Run;
end.

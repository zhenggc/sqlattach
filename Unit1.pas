unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,
   ComCtrls,StdCtrls,
   Buttons,
  DB, ADODB, FileCtrl, DBCtrls, Grids,
  DBGrids;

type
  TfrmAttach = class(TForm)
    AdoCnn: TADOConnection;
    ADOCommand1: TADOCommand;
    ADOQAttach: TADOQuery;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    DataSource1: TDataSource;
    FileName: TFileListBox;
    Dir: TDirectoryListBox;
    Driver: TDriveComboBox;
    Label1: TLabel;
    EdtServer: TEdit;
    Label2: TLabel;
    EdtUser: TEdit;
    Label3: TLabel;
    EdtPass: TEdit;
    BtnConnect: TButton;
    SBAttachDb: TSpeedButton;
    ADOQAdd: TADOQuery;
    Label4: TLabel;
    Edtpath: TEdit;
    OpDlg: TOpenDialog;
    FileListBox1: TFileListBox;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure BtnConnectClick(Sender: TObject);
    procedure SBAttachDbClick(Sender: TObject);
    procedure DirChange(Sender: TObject);
    procedure EdtpathChange(Sender: TObject);
    procedure OnDblClick(Sender: TObject);
    procedure DriverChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    G_dbFileName:string;
    G_LogFileName:string;
    function GetFileName( DbName :string ):boolean;
      function GetFilePath( DbName :string;logName:string ):string;
     procedure AddLog(Msg: string);
  end;

var
  frmAttach: TfrmAttach;

implementation

{$R *.dfm}

procedure TfrmAttach.AddLog(Msg:string);
var
  FileStream: TFileStream;
  LogFile: string;
  sFileName:string;
begin
  try
    Msg := '[' + DateTimeToStr(Now) + '] ' + Msg;
   // ServerLogs.Lines.Add(Msg);
     // mmlog.Lines.Add(msg);
      sfilename:=FormatDateTime('yyyy-mm-dd',now) ;       //格式化文件名；
    LogFile := ExtractFilePath(Application.ExeName) + '\Logs\' +sfilename + '.log';
    if not DirectoryExists(ExtractFilePath(LogFile)) then CreateDir(ExtractFilePath(LogFile));

    if FileExists(LogFile) then
      FileStream := TFileStream.Create(LogFile, fmOpenWrite or fmShareDenyNone)
    else
      FileStream := TFileStream.Create(LogFile, fmCreate or fmShareDenyNone);
    FileStream.Position := FileStream.Size;

    Msg := Msg + #13#10;
    FileStream.Write(PChar(Msg)^, Length(Msg));

    FileStream.Free;

  except
    on E: Exception do
    begin
         showmessage('写入日志失败！');
         //   ServerLogs.Lines.Add('写入日志失败：' + E.Message);
        // mmlog.Lines.Add('写入日志失败：' + E.Message);
    end;
  end;
end;

procedure TfrmAttach.BtnConnectClick(Sender: TObject);
begin
   if adocnn.Connected then
     adocnn.Close;
     //Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;Initial Catalog=master;Data Source=.;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=ZHENGGC;Use Encryption for Data=False;Tag with column collation when possible=False
            try                                                                 //Password='+ trim(edtpass.Text)  + ';
            begin
             adocnn.ConnectionString:= 'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=false;User ID='+
             trim(edtuser.Text ) +';Initial Catalog=master;Data Source='+ trim(edtserver.Text ) +';Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=' +trim(edtserver.Text ) +
             ';Use Encryption for Data=False;Tag with column collation when possible=False ';
              adocnn.Open();
            end;
            except
        on E: Exception     do ShowMessage('数据库连接失败！' +e.Message);

    // finally

         // showmessage('连接数据库失败！');
      end;
end;
// 添加
procedure TfrmAttach.Button1Click(Sender: TObject);
var i:integer;
begin
  if(self.FileListBox1.SelCount=0) then exit;
     for   i :=1 to filelistbox1.SelCount   do
      begin
          //filename.AddItem(filelistbox1.FileName,sender);

          filename.AddItem(ExtractFileName(filelistbox1.FileName),sender);
      end;
end;
   //删除
procedure TfrmAttach.Button2Click(Sender: TObject);
var
i : integer;
begin
   if(self.FileName.Count =0) then exit;
       if(self.filename.SelCount=0) then exit;
     for   i :=1 to filename.SelCount   do
      begin
          //filename.AddItem(filelistbox1.FileName,sender);
              filename.Items.Clear();
         // filename.AddItem(ExtractFileName(filelistbox1.FileName),sender);
      end;
end;


procedure TfrmAttach.DirChange(Sender: TObject);
begin
  //filename.Showing :=true;
   self.FileListBox1.Directory :=dir.Directory;
 // filename.Directory :=dir.Directory;
  edtpath.Text :=dir.Directory;
  //filename.Directory :='e:\data'        ;
end;

procedure TfrmAttach.DriverChange(Sender: TObject);
begin
  dir.Drive :=driver.Drive ;
end;

procedure TfrmAttach.EdtpathChange(Sender: TObject);
begin
      //  try
      //    filename.Directory :=  edtpath.Text;
      //  finally

      //  end;
end;

procedure TfrmAttach.FormCreate(Sender: TObject);
begin


//Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Initial Catalog=master;Data Source=.;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=ZHENGGC;Use Encryption for Data=False;Tag with column collation when possible=False
  //   adocnn.ConnectionString:='Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Initial Catalog=master;Data Source=.;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=ZHENGGC;Use Encryption for Data=False;Tag with column collation when possible=False';
end;

function tfrmattach.GetFilePath(DbName: string;logName:string):string;
var
LogPath :string;
begin
    logpath := ExtractFilePath(logname);
    getfilepath :=    ExtractFilePath(DbName)+copy(logname,length(logpath)+1,length(logname)-length(logpath));
end;

 function TfrmAttach.GetFileName( DbName :string ):boolean;
begin
      adoqattach.SQL.Clear;
      //adoqattach.SQL.Add();
      ADOQAttach.SQL.Add('DBCC checkprimaryfile(N'''+ DbName + ''', 3)' ) ;
 try
 ADOQAttach.Active :=true;
 while not self.ADOQAttach.Eof   do
   begin
   if   ADOQAttach.Fields[1].asstring   ='1'  then
      begin
             self.G_dbFileName :=trim(adoqattach.Fields[3].AsString);
            //  showmessage(self.G_dbFileName);
            AddLog('数据库文件:' + self.G_dbFileName);
      end;
         if   ADOQAttach.Fields[1].asstring   ='2'  then
      begin
             self.G_LogFileName :=trim(adoqattach.Fields[3].AsString);
              AddLog('日志文件:' + self.G_LogFileName);
           //  showmessage(self.G_LogFileName);
      end;
       self.ADOQAttach.Next;
   end;
     GetFileName :=true;
   except
     on E: Exception     do
     begin
      ShowMessage('检查数据文件出错！错误：' +e.Message + #13#10 + ' Sql:' +self.ADOQAttach.SQL.text);
      addlog('检查数据文件出错！错误：' +e.Message + #13#10 + ' Sql:' +self.ADOQAttach.SQL.text);
     end;

  end;
    
end;

procedure TfrmAttach.OnDblClick(Sender: TObject);
begin
     opdlg.filter :='*.mdf(数据库文件)|*.mdf';

     if opdlg.Execute then
      begin

        edtpath.Text := extractfilepath(opdlg.FileName);
         filelistbox1.Directory :=edtpath.Text;
         dir.Directory :=edtpath.Text ;
         driver.Drive :=dir.Drive;
      end;
end;

procedure TfrmAttach.SBAttachDbClick(Sender: TObject);
Var
  sPath :string;
  DbName:string;
  LogFileName:string;
  DbFileName:string;
  StrAttach:string;
    i:integer;
begin
{
DBCC checkprimaryfile(N'E:\data\CHSS_BS_HisClinci_XJ.MDF', 2)   --获得数据库名：
DBCC checkprimaryfile(N'E:\data\CHSS_BS_HisClinci_XJ.MDF', 3)   --获得数据库文件列表：
   --附加数据库
CREATE DATABASE [ERP] ON
( FILENAME = N'E:\手机版办公OA系统\代码\ERPData\ERP_Data.MDF' ),
( FILENAME = N'E:\手机版办公OA系统\代码\ERPData\ERP_Log.LDF' )
 FOR ATTACH
 }
  filename.SelectAll;
  //
  I :=0;

 for I := 0 to filename.Count - 1 do
     begin
     // inc(
     // inc(I)  ;
        //  filename.it
     if  filename.Selected[I] then
      begin
          spath  := dir.Directory + '\' + ExtractFileName(filename.FileName) ;
          addlog('选中第【'+ inttostr(i) +'】文件,选中文件名：' + spath);
      end;

  if trim(spath)='' then
   begin
      showmessage('请选择数据库目录！');
        exit;
   end;


  try
      self.ADOQAttach.SQL.Clear;
 self.ADOQAttach.SQL.Add('DBCC checkprimaryfile(N'''+ spath + ''', 2)' ) ;
 self.ADOQAttach.Active :=true;
 while not self.ADOQAttach.Eof   do
   begin
   if   ADOQAttach.Fields[0].asstring   ='Database name'  then
      begin
             dbname :=trim(adoqattach.Fields[1].AsString);
              addlog('附加后的数据库名：' + dbname);
             // filename.SelectAll;
           //  showmessage(dbname);
        if      GetFileName(spath)  then
         begin
           if  trim(spath)<>trim(self.G_dbFileName) then
                 begin
                     self.G_dbFileName :=trim(spath);
                     self.G_LogFileName :=getfilepath(spath,self.G_LogFileName);
                   //  showmessage(self.G_LogFileName);
                 end;
               adoqadd.SQL.Clear;
               StrAttach := 'CREATE DATABASE ['+ dbname +'] ON '  + #13#10
                  + '( FILENAME = N''' +self.G_dbFileName + ''' ),'   + #13#10
                   + '( FILENAME = N''' +self.G_LogFileName + ''' )'   + #13#10
                 +' FOR ATTACH '       ;
                 adoqadd.SQL.Add(strattach);
            try
                   adoqadd.ExecSQL;
                   addlog('附加数据库【'+dbname +'】成功！');

            except
             on E: Exception     do
                   begin
                     ShowMessage('附加数据库出错！错误：' +e.Message + #13#10 + ' Sql:' +self.ADOQAttach.SQL.text);
                     addlog('附加数据库出错！错误：' +e.Message + #13#10 + ' Sql:' +self.ADOQAttach.SQL.text);

                    end;
            end;
         end;
     self.ADOQAttach.Next;

   end;
   end;
   except
     on E: Exception     do
       begin
        ShowMessage('检查数据库名出错！错误：' +e.Message + #13#10 + ' Sql:' +self.ADOQAttach.SQL.text);
        addlog('检查数据库名出错！错误：' +e.Message + #13#10 + ' Sql:' +self.ADOQAttach.SQL.text);
       end;
   end;
     //inc(i);     
     end;

end;

end.
